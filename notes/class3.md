# Class 3

## Single-state MDP

We considered a farm that makes $100 a year. None of the money is spent and is instead saved in an account with a 2% interest rate. The goal is to decide whether to keep the farm or sell it for $5090 (and put the money in the bank) if the goal is to maximize profits over an infinite time period.

The solution is to use discounting (discount factor $`\gamma = 1/1.02`$). The python code that compares the options is [here](../solutions/farming/simple.py).

## Two state MDP

Consider a farm that makes $200 in an El Nino year and loses $20 in a La Nina year. The probability of staying in the El Nino state is 0.8, and the probability of staying in the La Nina state is 0.6. Do you sell the farm for $6300.

The solution using policy evaluation is in [here](../solutions/farming/simple.py).
