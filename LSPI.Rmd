---
title: "Lspi-G2"
author: "Yi Wang"
date: "10/17/2018"
output: pdf_document
---

```{r}
library(MDPtoolbox)

features <- matrix(c(-2,9,4, 3,7,-5, 3,6,1),nrow = 3,ncol = 3,byrow = TRUE)
reward <- matrix(c(10,-2,5))
lammda = 1/1.02
P <- array(0, c(3,3,2))
P[,,1] <- matrix(c(0.5, 0.5,0, 0.8, 0.2,0,0.3,0.7,0), 3, 3, byrow=TRUE)
P[,,2] <- matrix(c(0, 1,0, 0.1, 0.9,0,0.4,0.6,0), 3, 3, byrow=TRUE)
R <- matrix(c(5, 10, -1), 3, 2)
mdp_policy_iteration_modified(P, R,lammda)
weights <- matrix(c(0.2,0.2,0.6),nrow=3,ncol=1)
weight <- matrix(c(0,0,1),nrow=3,ncol=1)

B= t(features)%*%features - lammda*t(features)%*%(P[,,1]%*%features)
B2= t(features)%*%features - lammda*t(features)%*%(P[,,2]%*%features)
b<- t(features)%*%R[,1]
n =3

Q = matrix(0,3,3)
while (sum(weights == weight) != 3){
  weights=weight
  # Q = t(weight)%*%features
  # Q = t(weight)%*%t(features)
  
  for (k in 1:3){
    Q[,k] = weight[k]*features[,k]
  }
  Policy = max.col(Q)
  
  for(i in 1:3){
    for(j in 1:3){
      B[i,j] = B[i,j] + features[i,]%*%features[,j] - lammda*features[i,] %*% diag(features[,Policy])
      b[i]=b[i]+ R[,1] %*% features[i,] 
    }
  weight=solve(B)%*%b
  }
  print(weights)
}




```

