# Implementation Instructions: Value Iteration, Policy Iteration, Modified Policy Iteration

The goal is the implement the algorithm described:

- Group G1: Value iteration
- Group G2: Policy iteration
- Group G3: Modified policy iteration

## Objective

The specific objective is to solve a **discounted infinite** horizon Markov decision process. The number of states and actions will be relatively small (<1000). The transition probabilities and rewards are provided in the form of a CSV file. 

## MDP Specification

Assume the following representation of the MDP:

- The states are actions are discrete and are identified by integers (state ID, action ID)
- State IDs are numeric, 0-based. An example {0,1,2,3}
- Action IDs are also numeric and 0-based. An example is {0,1} if there are two actions. 
- The number of actions may vary from state to state.
- Rewards depend on state, action, *and the next state*

No need to worry too much about computational complexity. In practice,using sparse representations of the transition probabilities may make a big difference, but that is not the point right now.

## Input

The input is a CSV (comma separated values) file. The columns and a short example are:

```
idstatefrom,idaction,idstateto,probability,reward
0,0,0,0.3,1.0
0,0,1,0.2,1.5
0,0,2,0.1,1.8
0,0,3,0.4,2.0
```
The first line means that when you take action 0 in state 0, you transition back to state 0 with probability 0.0.3, and the reward that is received is 1.0.

Let $`\gamma`$ be the discount rate, then the optimal value function of the example above should be:
```math
v = \frac{1}{1-\gamma} (0.3\cdot 1.0 + 0.2 \cdot 1.5 + 0.1 \cdot 1.8 + 0.4 \cdot 2.0)
```

*Hint*: Use pandas to load the CSV file into python.

You can test your code to make sure that it solves the MDP available from [here](http://rmdp.xyz:3838/cs980/inventory/). However, it is difficult to use this more complex problem to make sure that your solutions are correct. So test it on small examples where you know what the solutions should be.

## Output

The output should be a CSV of a deterministic policy, such as

```
idstate,idaction
0,1
1,2
2,0
```
*Hint*: Use pandas to write the CSV from python.

## Implementation considerations

The focus should be on:

- *Correctness*: I strongly encourage unit tests of some form. At least, you should construct some small examples and test the algorithm on them. Also try an example in which the number of actions varies from state to state.
- *Succintness*: Fewer lines of code are better. Document the code well.


## Presentation

- Please prepare a presentation of the algorithm and whatever relevant properties you think are important
- What is the computational complexity, why does this method work, etc
- Describe the implementation, the choices and trade-offs, and what could have been done differently
- We will try solve this [Inventory management problem](http://rmdp.xyz:3838/cs980/inventory/) in the class using your code
